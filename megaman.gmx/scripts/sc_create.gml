grav = 0.2;
hsp = 0;
vsp = 0;
global.points = 0;

movespeed = 4;

jumpspeed_normal = 5;
jumpspeed_powerup = 10;

jumpspeed = jumpspeed_normal;

//IA Checks
hcollision = false;
fearofheights = true;
dir = 0;

//Constants
grounded = false;
jumping = false;

//Horizontal jump constants
hsp_jump_constant_small = 1;
hsp_jump_constant_big = 4;
hsp_jump_applied = 0;

//Horizontal key pressed count
hkp_count = 0;
hkp_count_small = 2;
hkp_count_big = 5;

//Init variables
key_left = 0;
key_right = 0;
key_jump = false;

//Init variables
key_left = 0;
key_right = 0;
key_up = 0;
key_down = 0;
key_jump = false;

//sprites

spriteStand = spr_player;
spriteWalk = spr_player_walking;
spriteJump = spr_player_jump;
spriteFall = spr_player_fall;
spriteStairs = spr_player_stairs;
